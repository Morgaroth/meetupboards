package controllers

import com.mongodb.casbah.commons.MongoDBObject
import controllers.Application._
import controllers.Boards._
import controllers.OAuth2.{TOKEN_SESSION, NAME_SESSION, ID_SESSION}
import models.db._
import models.{Event, EventParser}
import play.api.Logger
import play.api.libs.json.Json
import play.api.libs.ws.WS
import play.api.mvc._
import utils.{invalidSession, Headers}

import scala.concurrent.Future

object Users extends Controller with EventParser with FollowerForm {
  implicit val context = play.api.libs.concurrent.Execution.Implicits.defaultContext

  import play.api.Play.current

  def list = Action { implicit request =>
    if (invalidSession(request)) {
      Redirect("/").withNewSession
    } else {
      Ok(views.html.users_list(UserDAO.find(MongoDBObject.empty).toList))
    }
  }

  def info(userId: String) = Action { implicit request =>
    if (invalidSession(request)) {
      BadRequest("notAuthorized").withNewSession
    } else {
      UserDAO.findOneById(userId) match {
        case Some(user) =>
          Ok(views.html.user_info(user, BoardDAO.find(MongoDBObject("ownerId" -> user.id)).toList))
        case None =>
          NotFound
      }
    }
  }

  def follow(userId: String) = Action {
    implicit request =>
      if (invalidSession(request)) {
        Results.Redirect("/").withNewSession.flashing(
          "failure" -> "invalid session"
        )
      } else {
        followerForm.bindFromRequest.fold(
          formWithErrors => {
            Redirect(routes.Users.info(userId)).flashing(
              "failure" -> "invalid form",
              "errors" -> formWithErrors.errors.mkString
            ).withSession(request.session)
          },
          correctForm => {
            UserDAO.findOneById(userId) match {
              case Some(user) =>
                UserDAO.save(user.copy(followers = correctForm :: user.followers))
                Redirect(routes.Users.info(userId)).withSession(request.session)
              case None =>
                NotFound.withSession(request.session)
            }
          }
        )
      }
  }
}
