package utils

import play.api.http.HeaderNames

object Headers extends Headers {
  val Authorization = HeaderNames.AUTHORIZATION
}

trait Headers {
  def MUAuth(authToken: String) = Headers.Authorization -> s"bearer $authToken"
}