import play.PlayScala
import xsbti.Predefined

name := """MeetupBoards"""

version := "0.1.0-SNAPSHOT"

scalaVersion := "2.10.4"

libraryDependencies ++= Seq(
  "org.postgresql" % "postgresql" % "9.3-1102-jdbc41",
  "io.spray" %% "spray-json" % "1.3.1",
  "io.github.morgaroth" %% "morgaroth-utils-mongodb" % "1.2.5"
)

lazy val MeetupBoards = (project in file(".")).enablePlugins(PlayScala)

libraryDependencies ++= Seq(
  cache,
  json,
  ws
)
