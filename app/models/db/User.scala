package models.db

import com.novus.salat.annotations.Key
import com.novus.salat.global._
import io.github.morgaroth.utils.mongodb.salat.SalatDAOWithCfg
import play.api.data.Form
import play.api.data.Forms._

case class Follower(id: String, name: String)


trait FollowerForm {
  val followerForm = Form(
    mapping(
      "followerId" -> text,
      "followerName" -> text
    )(Follower.apply)(Follower.unapply)
  )
}

case class User(
                 @Key("_id") id: String,
                 name: String,
                 followers: List[Follower]
                 )

object UserDAO extends SalatDAOWithCfg[User, String]("app.mongo.uri", "meetupboards_users")