package utils

import controllers.OAuth2
import play.api.mvc.Request

object invalidSession {
  def apply[T](request: Request[T]): Boolean = {
    !request.session.data.contains(OAuth2.TOKEN_SESSION) ||
      !request.session.data.contains(OAuth2.ID_SESSION) ||
      !request.session.data.contains(OAuth2.NAME_SESSION)
  }
}
