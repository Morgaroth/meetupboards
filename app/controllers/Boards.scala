package controllers

import com.mongodb.casbah.commons.MongoDBObject
import controllers.Application._
import controllers.OAuth2.{TOKEN_SESSION, NAME_SESSION, ID_SESSION}
import models.db.{UserDAO, CommentForm, Board, BoardDAO}
import models.{Event, EventParser}
import play.api.Logger
import play.api.libs.json.Json
import play.api.libs.ws.WS
import play.api.mvc._
import play.api.mvc.Results._
import utils.{invalidSession, Headers}

import scala.concurrent.Future

object Boards extends Controller with EventParser with CommentForm {
  implicit val context = play.api.libs.concurrent.Execution.Implicits.defaultContext

  import play.api.Play.current

  def info(boardId: String) = Action.async { implicit request =>
    if (invalidSession(request)) {
      Future(BadRequest("notAuthorized").withNewSession)
    } else {
      BoardDAO.findOneById(boardId) match {
        case Some(board) =>
          WS.url(s"https://api.meetup.com/2/event/${board.eventId}").withHeaders {
            Headers.MUAuth(request.session(TOKEN_SESSION))
          }.withQueryString(
              "member_id" -> "self"
          ).get().map { response =>
            Logger.info(s"received response from event id $boardId response $response, entity ${response.body}")
            val json = Json.parse(response.body)
            val event = json.as[Event]
            Logger.info(s"received event $event")
            Ok(views.html.board_info(request.session(NAME_SESSION), event, board))
          }
        case None =>
          Future(NotFound)
      }
    }
  }

  def create(eventId: String) = Action { implicit request =>
    if (invalidSession(request)) {
      BadRequest("notAuthorized").withNewSession
    } else {
      val boardOpt = BoardDAO.findOneById(s"$eventId-${request.session(ID_SESSION)}")
      val userOpt = UserDAO.findOneById(request.session(ID_SESSION))
      (boardOpt, userOpt) match {
        case (None, None) =>
          Redirect("/").withNewSession.flashing(
            "failure" -> "user not exists"
          )
        case (Some(board), None) =>
          Redirect(routes.Boards.info(board.boardId)).withNewSession.flashing(
            "failure" -> "user not exists"
          )
        case (Some(board), Some(user)) if user.id == board.ownerId =>
          Redirect(routes.Boards.info(board.boardId)).withNewSession.flashing(
            "failure" -> "Your board is existing"
          )
        case (None, Some(user)) =>
          val board = Board(s"$eventId-${request.session(ID_SESSION)}", eventId, request.body.asFormUrlEncoded.get.get("eventName").get.head, user.id, user.name, List.empty)
          BoardDAO.save(board)
          Logger.info(s"saved new board $board")
          Redirect(routes.Boards.info(board.boardId)).withSession(request.session)
      }
    }
  }


  def comment(boardId: String) = Action {
    implicit request =>
      if (invalidSession(request)) {
        Results.Redirect("/").withNewSession.flashing(
          "failure" -> "invalid session"
        )
      } else {
        commentForm.bindFromRequest.fold(
          formWithErrors => {
            Redirect(routes.Boards.info(boardId)).flashing(
              "failure" -> "The item has been created",
              "errors" -> formWithErrors.errors.mkString
            ).withSession(request.session)
          },
          commentForm => {
            BoardDAO.findOneById(boardId) match {
              case Some(board) =>
                BoardDAO.save(board.copy(comments = commentForm :: board.comments))
                Redirect(routes.Boards.info(boardId)).withSession(request.session)
              case None =>
                NotFound.withSession(request.session)
            }
          }
        )
      }
  }
  def list = Action {
    implicit request =>
      if (invalidSession(request)) {
        Redirect("/").withNewSession
      } else {
        Ok(views.html.boards_list(BoardDAO.find(MongoDBObject.empty).toList)).withSession(request.session)
      }
  }
}
