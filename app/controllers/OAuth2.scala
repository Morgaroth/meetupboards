package controllers

import models.db.{User, UserDAO}
import models.{UserID, UserTokenJsonSupp}
import play.api.Logger
import play.api.libs.json.Json
import play.api.libs.ws.{WSResponse, WS}
import play.api.mvc.{Result, Action, Controller, Results}
import spray.json._
import utils.Headers

import scala.concurrent.Future

object OAuth2 extends Controller with UserTokenJsonSupp {

  val TOKEN_SESSION: String = "token"
  val ID_SESSION: String = "id"
  val NAME_SESSION: String = "name"

  import play.api.Play.current

  implicit val context = play.api.libs.concurrent.Execution.Implicits.defaultContext
  def authenticated(code: Option[String], state: Option[String], error: Option[String]) = Action.async { implicit req =>
    Logger.error(s"received code=$code with state=$state and error=$error")
    Logger.error(s"received body ${req.body}, headers=${req.headers.toMap}, cookies=${req.cookies.toList}")
    (code, state, error) match {
      case (Some(codeVal), Some(stateVal), None) =>
        WS.url("https://secure.meetup.com/oauth2/access").post(Map(
          "client_id" -> Seq(Application.key),
          "client_secret" -> Seq(Application.secret),
          "grant_type" -> Seq("authorization_code"),
          "redirect_uri" -> Seq(Application.domain),
          "code" -> Seq(codeVal)
        )).flatMap { (response: WSResponse) =>
          response.status match {
            case succ if succ < 400 =>
              val token = UserTokenReqJsonSerializer.read(response.body.parseJson)
              WS.url("https://api.meetup.com/2/members").withHeaders {
                Headers.MUAuth(token.access_token)
              }.withQueryString {
                "member_id" -> "self"
              }.get().map { response =>
                Logger.info(s"received response from members $response, code ${response.status}")
                Logger.error(s"response json ${response.body}")
                if (response.status < 400) {
                  val parsed = Json.parse(response.body)
                  val userID = (parsed \ "results")(0) \ "id"
                  val userName = (parsed \ "results")(0) \ "name"
                  Logger.info(s"readed userID=$userID and user name = $userName")
                  Some(User(userID.as[Long].toString, userName.as[String], List.empty))
                } else None
              }.map {
                case Some(userId) =>
                  UserDAO.save(userId)
                  Results.Redirect("/").withSession(
                    TOKEN_SESSION -> token.access_token,
                    ID_SESSION -> userId.id.toString,
                    NAME_SESSION -> userId.name
                  )
                case None =>
                  Results.Redirect("/").withNewSession
              }
            case fail =>
              Future(Results.Ok(s"fail response: $response"))
          }
        }
      case (None, None, Some(explanation)) =>
        Future(Results.BadRequest)
      case _ =>
        Future(Results.InternalServerError("WTF?"))
    }
  }

  def logout = Action {
    Results.Redirect("/").withNewSession
  }
}