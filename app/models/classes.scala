package models

import play.api.libs.json.{Reads, JsPath}
import play.api.libs.functional.syntax._

case class UserID(id: Long, name: String)

case class EventData(name: String, id: String)

case class Venue(
                  address: String,
                  city: String,
                  country: String
                  )

case class Event(
                  status: String,
                  visibility: String,
                  event_url: String,
                  description: String,
                  time: Long,
                  duration: Long,
                  name: String,
                  venue: Venue
                  )
object Venue {
  def undefined: Venue = Venue("undefined address", "undefined city", "undefined country")

  def generate(venueAddress: Option[String],
               venueCity: Option[String],
               venueCountry: Option[String]) =
    Venue(
      address = venueAddress.getOrElse("undefined venueAddress"),
      city = venueCity.getOrElse("undefined venueCity"),
      country = venueCountry.getOrElse("undefined venueCountry")
    )
}

trait VenueParser {
  implicit val venueReads: Reads[Venue] = (
    (JsPath \ "address_1").readNullable[String] and
      (JsPath \ "city").readNullable[String] and
      (JsPath \ "country").readNullable[String]
    )(Venue.generate _)

}

object Event {
  def generate(status: Option[String],
               visibility: Option[String],
               event_url: Option[String],
               description: Option[String],
               time: Option[Long],
               duration: Option[Long],
               name: Option[String],
               venue: Option[Venue]) =
    Event(
      status = status.getOrElse("undefined status"),
      visibility = visibility.getOrElse("undefined visibility"),
      event_url = event_url.getOrElse("undefined event_url"),
      description = description.getOrElse("undefined description"),
      time = time.getOrElse(-1),
      duration = duration.getOrElse(-1),
      name = name.getOrElse("undefined name"),
      venue = venue.getOrElse(Venue.undefined)
    )
}
trait EventParser extends VenueParser {
  implicit val eventReads: Reads[Event] = (
    (JsPath \ "status").readNullable[String] and
      (JsPath \ "visibility").readNullable[String] and
      (JsPath \ "event_url").readNullable[String] and
      (JsPath \ "description").readNullable[String] and
      (JsPath \ "time").readNullable[Long] and
      (JsPath \ "duration").readNullable[Long] and
      (JsPath \ "name").readNullable[String] and
      (JsPath \ "venue").readNullable[Venue]
    )(Event.generate _)
}