package models.db

import com.novus.salat.annotations.Key
import io.github.morgaroth.utils.mongodb.salat.SalatDAOWithCfg
import com.novus.salat.global._
import play.api.data._
import play.api.data.validation.Constraints._
import play.api.data.Forms._

case class Comment(
                    text: String,
                    commenterId: String,
                    commenterName: String
                    )
trait CommentForm {
  val commentForm = Form(
    mapping(
      "text" -> text.verifying(nonEmpty),
      "commenterId" -> text,
      "commenterName" -> text
    )(Comment.apply)(Comment.unapply)
  )
}
case class Board(
                  @Key("_id") boardId: String,
                  eventId: String,
                  eventName: String,
                  ownerId: String,
                  ownerName: String,
                  comments: List[Comment]
                  )

object BoardDAO extends SalatDAOWithCfg[Board, String]("app.mongo.uri", "meetupboards_boards") {

}