package controllers

import models.EventData
import play.api.Logger
import play.api.libs.json.{JsValue, Json}
import play.api.libs.ws.WS
import play.api.mvc._
import utils.{invalidSession, Headers}

import scala.concurrent.Future

object Application extends Controller {
  val apiKey = "d4b151616428156462e121526621f"
  val key = "s9ln5mubucn0j72i6ulg5db2ep"
  val secret = "qr9vbsvls9pmhur3ipk6ff2o1s"
  val domain = "http://meetupboards.herokuapp.com/internal/oauth"

  import play.api.Play.current

  implicit val context = play.api.libs.concurrent.Execution.Implicits.defaultContext

  def index = Action.async { implicit request =>
    if (invalidSession(request)) {
      Future(Ok(views.html.index_unauthorized("hello", key, domain)))
    } else {
      WS.url("https://api.meetup.com/2/events").withHeaders {
        Headers.MUAuth(request.session(OAuth2.TOKEN_SESSION))
      }.withQueryString(
          "member_id" -> "self",
          "page" -> "100"
        ).get().map { response =>
        Logger.info(s"received response from events $response")
        val json = Json.parse(response.body)
        val events = (json \ "results").as[Array[JsValue]]
        val eventsData: Array[EventData] = events.map(ev => {
          val name = ev \ "name"
          val id = ev \ "id"
          EventData(name.as[String], id.as[String])
        })
        Logger.info(s"received events list size ${eventsData.length}")
        Ok(views.html.index_authorized(request.session(OAuth2.NAME_SESSION), eventsData)).withSession(request.session)
      }
    }
  }
}