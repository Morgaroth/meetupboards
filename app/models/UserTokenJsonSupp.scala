package models

import spray.json.DefaultJsonProtocol

trait UserTokenJsonSupp extends DefaultJsonProtocol {
  case class UserTokenReq(token_type: String, expires_in: Long, refresh_token: String, access_token: String)
  implicit val UserTokenReqJsonSerializer = jsonFormat4(UserTokenReq)
}
